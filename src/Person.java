/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author urwah
 */
 
 
public class Person {
     private String name;
    private double weight;
    private double height;
    Weight unitWeight;
    Height unitHeight;
    
      Person(String name){
          this.name=name;
      }
    public Person(String name, double weight, double height, Weight unitWeight, Height unitHeight){
        this.name=name;
        this.weight=weight;
        this.height=height;
        this.unitWeight=unitWeight;
        this.unitHeight=unitHeight;
    }

    public String getName(){
        return this.name;
    }
    
    public double getWeight(){
        return this.weight;
    }
    
    public double getHeight(){
        return this.height;
    }
    
    public Weight getUnitweight(){
        return this.unitWeight;
    }
    public Height getUnitheight(){
        return this.unitHeight;
    }
    
    public double getBMI(){
        double weightInKilos=0;
         double heightInMeters=0;
      //  if (unitWeight == Weight.K && unitHeight == Height.M ){
          //  return weight /(Math.pow(height, 2));}
        
        switch(unitWeight){
          case K : weightInKilos = weight;
          break;
          case LB : weightInKilos = weight * 0.4535;
          break;
        }
        
        switch(unitHeight){
          case  M : heightInMeters = height;
          break;
          case  IN : heightInMeters = height * 0.0254;
          break;
        }
        return weightInKilos / (Math.pow(heightInMeters,2));
    }
   

    public String toString(){
        return "Name:"+name+" Weight: "+weight+unitWeight+"  Height:"+height+unitHeight;
    }
    
}
